/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Domain;

/**
 * enum with the possible values of Type of Permissions
 * @author David
 */
public enum TypeOfPermission {
    Read, Write, FullControll
}
