/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import Bootstrap.Bootstrap;
import Domain.Directory;
import Domain.Disk;
import Domain.File;
import Users.User;
import java.io.IOException;
import static java.lang.System.exit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author user
 */
public class AdminMenuUI {

    private Disk initDisk;
    private Directory parentDirectory;
    private User userSession;
    private DirectoryUI dUI;
    private Directory cutDirectory;
    private FileUI fUI;

    public AdminMenuUI(User userSession) {
        this.initDisk = Bootstrap.initializeDisk();
        this.parentDirectory = this.initDisk.getRoot();
        this.userSession = userSession;
        this.dUI = new DirectoryUI(initDisk.getRoot(), this.userSession);
        this.fUI = new FileUI(initDisk.getRoot(), this.userSession);
    }

    public void show() throws IOException, Exception {

        System.out.print(System.lineSeparator()
                + Calendar.getInstance().getTime()
                + System.lineSeparator());

        System.out.println("## Welcome filesystem ##" + System.lineSeparator());
        List<String> options = new ArrayList<String>();

        Scanner scanIn = new Scanner(System.in);

        String choice;
        do {

            System.out.println("Please choose your option:");
            System.out.println(
                    "#1 - List Directories inside your current directory#         "
                    + "#2 - List Files inside your current directory#          "
                    + "#3 - Navigate to a directory inside your current directory#\n"
                    + "#4 - Create new directory inside your current directory#     "
                    + "#5 - Cut directory#                                     "
                    + "#6 - Copy directory#\n"
                    + "#7 - Paste directory#                                        "
                    + "#8 - Delete Directory#                                  "
                    + "#9 - Navigate back to Parent Directory#\n"
                    + "#10 - Create new File#                                       "
                    + "#11 - Delete File#                                      "
                    + "#12 - Edit Permissions of a File#\n"
                    + "#13 - Check details of a File#                               "
                    + "#14 - Copy a File#                                      "
                    + "#15 - Paste a File#\n"
                    + "#16 - Change content of a File#                              "
                    + "#17 - Cut a File#"
                    
                    + "\n0 - Logout\n");

            choice = scanIn.nextLine();

            switch (choice) {
                case "0":
                    System.out.println("Bye Bye :D");
                    exit(0);
                    break;
                case "1":
                    UIUtils.showDirectories(this.parentDirectory);
                    break;
                case "2":
                    UIUtils.showFiles(this.parentDirectory);
                    break;

                case "3":
                    Directory aux3=this.parentDirectory;
                    String dirName = UIUtils.readLineFromConsole("Introduce your desired directory name:" + String.format("%n>"));
                    this.parentDirectory = UIUtils.getDirectoryByName(this.parentDirectory, dirName);
                    if(this.parentDirectory!=null){
                    System.out.println("You are now in directory " + this.parentDirectory.getDirectoryName());
                    }else{
                        System.out.println("The introduced name: "+dirName+" is not a directory.");
                        this.parentDirectory=aux3;
                    }
                    break;

                case "4":
                    this.dUI.askInformation(this.parentDirectory);
                    
                    break;

                case "5":
                    this.dUI.askInformationToCutDirectory(this.parentDirectory);
                    break;

                case "6":
                    this.dUI.askInformationToCopyADirectory(this.parentDirectory);
                    break;

                case "7":
                    Directory aux=this.parentDirectory;
                    
                    if(!this.dUI.pasteDirectory(this.parentDirectory)){
                        this.parentDirectory=aux;
                    }
                    break;

                case "8":
                    this.dUI.askInformationToDeleteDirectory(this.parentDirectory);
                    break;

                case "9":
                    this.parentDirectory = this.parentDirectory.getParentDirectory();
                    if(this.parentDirectory!=null){
                        System.out.println("You are now in directory " + this.parentDirectory.getDirectoryName());
                    }else{
                        System.out.println("You already are in root directory");
                    }
                    break;

                case "10":
                    this.fUI.askInformation(this.parentDirectory);
                    break;

                case "11":
                    this.dUI.askInformationToDeleteFile(this.parentDirectory);
                    break;

                case "12":
                    this.fUI.changePermissionsOfAFile(this.parentDirectory);
                    break;

                case "13":
                    this.fUI.checkDetailsOfAFile(this.parentDirectory);
                    break;
                
                case "14":
                    this.dUI.askInformationToCopyAFile(this.parentDirectory);
                    break;
                    
                 case "15":
                    Directory aux1=this.parentDirectory;
                    if(!this.dUI.pasteFile(this.parentDirectory)){
                        this.parentDirectory=aux1;
                    }
                    break;
                    
                  case "16":
                    this.fUI.changeContentOfAFile(this.parentDirectory);
                    break;
                    
                  case "17":
                    this.dUI.askInformationToCutFile(this.parentDirectory);
                    break;
                    
                

            }
        } while (!choice.equals("0"));
    }

}
