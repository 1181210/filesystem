/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Domain.Directory;
import Domain.File;
import Domain.Permissions;
import Domain.Roles;
import Domain.TypeOfPermission;
import Users.User;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author user
 */
public class DirectoryControllerIT {

    public DirectoryControllerIT() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of createDirectory method, of class DirectoryController.
     */
    @Test
    public void testCreateDirectory() {
        System.out.println("createDirectory");
        String name = "newDir";
        String path = "/DirectoryParent/newDir";
        List<Permissions> l = new ArrayList<Permissions>();
        Permissions e = new Permissions(Roles.USER, TypeOfPermission.Write);
        l.add(e);
        DirectoryController instance = new DirectoryController(new User("Admin", "Admin", Roles.ADMIN));
        Directory expResult = new Directory(name, new ArrayList<File>(), new ArrayList<Directory>(), path, l);
        Directory result = instance.createDirectory(name, path, l);
        assertEquals(expResult, result);

    }

    /**
     * Test of verifyPermissions method, of class DirectoryController.
     */
    @Test
    public void testVerifyPermissionsFalse() {
        System.out.println("verifyPermissionsFalse");
        String name = "newDir";
        String path = "/DirectoryParent/newDir";
        List<Permissions> l = new ArrayList<Permissions>();
        Permissions e = new Permissions(Roles.USER, TypeOfPermission.Read);
        l.add(e);
        Directory parentDirectory = new Directory(name, new ArrayList<File>(), new ArrayList<Directory>(), path, l);
        DirectoryController instance = new DirectoryController(new User("Admin", "Admin", Roles.ADMIN));
        boolean expResult = false;
        boolean result = instance.verifyPermissions(parentDirectory);
        assertEquals(expResult, result);

    }

    /**
     * Test of verifyPermissions method, of class DirectoryController.
     */
    @Test
    public void testVerifyPermissionsTrue() {
        System.out.println("verifyPermissionsTrue");
        String name = "newDir";
        String path = "/DirectoryParent/newDir";
        List<Permissions> l = new ArrayList<Permissions>();
        Permissions e = new Permissions(Roles.ADMIN, TypeOfPermission.FullControll);
        l.add(e);
        Directory parentDirectory = new Directory(name, new ArrayList<File>(), new ArrayList<Directory>(), path, l);
        DirectoryController instance = new DirectoryController(new User("Admin", "Admin", Roles.ADMIN));
        boolean expResult = true;
        boolean result = instance.verifyPermissions(parentDirectory);
        assertEquals(expResult, result);

    }

    /**
     * Test of addDirectory method, of class DirectoryController.
     */
    @Test
    public void testAddDirectory() {
        System.out.println("addDirectory");
        String name = "newDir";
        String path = "/DirectoryParent";
        List<Permissions> l = new ArrayList<Permissions>();
        Permissions e = new Permissions(Roles.ADMIN, TypeOfPermission.FullControll);
        l.add(e);
        Directory parentDirectory = new Directory(name, new ArrayList<File>(), new ArrayList<Directory>(), path, l);
        Directory childDirectory = new Directory("ChildDirectory", new ArrayList<File>(), new ArrayList<Directory>(), "DirectoryParent/ChildDirectory", l);
        DirectoryController instance = new DirectoryController(new User("Admin", "Admin", Roles.ADMIN));
        int expResult = 1;
        instance.addDirectory(parentDirectory, childDirectory);
        int result = parentDirectory.getChildrenDirectory().size();
        assertEquals(expResult, result);

    }

    /**
     * Test of removeDirectory method, of class DirectoryController.
     */
    @Test
    public void testRemoveDirectory() {
        System.out.println("removeDirectory");
        String name = "newDir";
        String path = "/DirectoryParent";
        List<Permissions> l = new ArrayList<Permissions>();
        Permissions e = new Permissions(Roles.ADMIN, TypeOfPermission.FullControll);
        l.add(e);
        Directory parentDirectory = new Directory(name, new ArrayList<File>(), new ArrayList<Directory>(), path, l);
        Directory childDirectory = new Directory("ChildDirectory", new ArrayList<File>(), new ArrayList<Directory>(), "DirectoryParent/ChildDirectory", l);
        parentDirectory.getChildrenDirectory().add(childDirectory);
        DirectoryController instance = new DirectoryController(new User("Admin", "Admin", Roles.ADMIN));
        boolean expResult = true;
        boolean result = instance.removeDirectory(parentDirectory, "ChildDirectory");
        assertEquals(expResult, result);

    }

    /**
     * Test of removeDirectory method, of class DirectoryController.
     */
    @Test
    public void testRemoveDirectoryFalse() {
        System.out.println("removeDirectoryFalse");
        String name = "newDir";
        String path = "/DirectoryParent";
        List<Permissions> l = new ArrayList<Permissions>();
        Permissions e = new Permissions(Roles.ADMIN, TypeOfPermission.FullControll);
        l.add(e);
        Directory parentDirectory = new Directory(name, new ArrayList<File>(), new ArrayList<Directory>(), path, l);
        Directory childDirectory = new Directory("ChildDirectory", new ArrayList<File>(), new ArrayList<Directory>(), "DirectoryParent/ChildDirectory", l);
        parentDirectory.getChildrenDirectory().add(childDirectory);
        DirectoryController instance = new DirectoryController(new User("Admin", "Admin", Roles.ADMIN));
        boolean expResult = false;
        boolean result = instance.removeDirectory(parentDirectory, "Chil");
        assertEquals(expResult, result);

    }

    /**
     * Test of removeFile method, of class DirectoryController.
     */
    @Test
    public void testRemoveFile() {
        System.out.println("removeFile");
        String name = "newDir";
        String path = "/DirectoryParent";
        List<Permissions> l = new ArrayList<Permissions>();
        Permissions e = new Permissions(Roles.ADMIN, TypeOfPermission.FullControll);
        l.add(e);
        Directory parentDirectory = new Directory(name, new ArrayList<File>(), new ArrayList<Directory>(), path, l);
        byte[] b = new byte[5];
        File file = new File("FileTest.txt", b, "DirectoryParent/FileTest", l);
        parentDirectory.getChildrenFile().add(file);
        String childFile = "FileTest";
        DirectoryController instance = new DirectoryController(new User("Admin", "Admin", Roles.ADMIN));
        boolean expResult = true;
        boolean result = instance.removeFile(parentDirectory, childFile);
        assertEquals(expResult, result);
    }

    /**
     * Test of removeFile method, of class DirectoryController.
     */
    @Test
    public void testRemoveFileFail() {
        System.out.println("removeFileFail");
        String name = "newDir";
        String path = "/DirectoryParent";
        List<Permissions> l = new ArrayList<Permissions>();
        Permissions e = new Permissions(Roles.ADMIN, TypeOfPermission.FullControll);
        l.add(e);
        Directory parentDirectory = new Directory(name, new ArrayList<File>(), new ArrayList<Directory>(), path, l);
        byte[] b = new byte[5];
        File file = new File("FileTest.txt", b, "DirectoryParent/FileTest", l);
        parentDirectory.getChildrenFile().add(file);
        String childFile = "FileTest123";
        DirectoryController instance = new DirectoryController(new User("Admin", "Admin", Roles.ADMIN));
        boolean expResult = false;
        boolean result = instance.removeFile(parentDirectory, childFile);
        assertEquals(expResult, result);
    }

    /**
     * Test of cutDirectory method, of class DirectoryController.
     */
    @Test
    public void testCutDirectory() {
        System.out.println("cutDirectory");
        String name = "newDir";
        String path = "/DirectoryParent";
        List<Permissions> l = new ArrayList<Permissions>();
        Permissions e = new Permissions(Roles.ADMIN, TypeOfPermission.FullControll);
        l.add(e);
        Directory parentDirectory = new Directory(name, new ArrayList<File>(), new ArrayList<Directory>(), path, l);
        Directory childDirectory = new Directory("ChildDirectory", new ArrayList<File>(), new ArrayList<Directory>(), "DirectoryParent/ChildDirectory", l);
        parentDirectory.getChildrenDirectory().add(childDirectory);
        DirectoryController instance = new DirectoryController(new User("Admin", "Admin", Roles.ADMIN));
        Directory expResult = childDirectory;
        Directory result = instance.cutDirectory(parentDirectory, "ChildDirectory");
        assertEquals(expResult, result);

    }

    /**
     * Test of cutDirectory method, of class DirectoryController.
     */
    @Test
    public void testCutDirectoryFail() {
        System.out.println("cutDirectoryFail");
        String name = "newDir";
        String path = "/DirectoryParent";
        List<Permissions> l = new ArrayList<Permissions>();
        Permissions e = new Permissions(Roles.ADMIN, TypeOfPermission.FullControll);
        l.add(e);
        Directory parentDirectory = new Directory(name, new ArrayList<File>(), new ArrayList<Directory>(), path, l);
        Directory childDirectory = new Directory("ChildDirectory", new ArrayList<File>(), new ArrayList<Directory>(), "DirectoryParent/ChildDirectory", l);
        parentDirectory.getChildrenDirectory().add(childDirectory);
        DirectoryController instance = new DirectoryController(new User("Admin", "Admin", Roles.ADMIN));
        Directory expResult = null;
        Directory result = instance.cutDirectory(parentDirectory, "ChildDirectory123");
        assertEquals(expResult, result);

    }

    /**
     * Test of pasteDirectory method, of class DirectoryController.
     */
    @Test
    public void testPasteDirectory() {
        System.out.println("pasteDirectory");
        String name = "newDir";
        String path = "/DirectoryParent";
        List<Permissions> l = new ArrayList<Permissions>();
        Permissions e = new Permissions(Roles.ADMIN, TypeOfPermission.FullControll);
        l.add(e);
        Directory parentDirectory = new Directory(name, new ArrayList<File>(), new ArrayList<Directory>(), path, l);
        Directory directoryToPaste = new Directory("ChildDirectory", new ArrayList<File>(), new ArrayList<Directory>(), "DirectoryParent/ChildDirectory", l);
        DirectoryController instance = new DirectoryController(new User("Admin", "Admin", Roles.ADMIN));
        boolean expResult = true;
        boolean result = instance.pasteDirectory(parentDirectory, directoryToPaste);
        assertEquals(expResult, result);

    }

    /**
     * Test of pasteDirectory method, of class DirectoryController.
     */
    @Test
    public void testPasteDirectoryFail() {
        System.out.println("pasteDirectoryFail");
        String name = "newDir";
        String path = "/DirectoryParent";
        List<Permissions> l = new ArrayList<Permissions>();
        Permissions e = new Permissions(Roles.ADMIN, TypeOfPermission.Read);
        l.add(e);
        Directory parentDirectory = new Directory(name, new ArrayList<File>(), new ArrayList<Directory>(), path, l);
        Directory directoryToPaste = new Directory("ChildDirectory", new ArrayList<File>(), new ArrayList<Directory>(), "DirectoryParent/ChildDirectory", l);
        DirectoryController instance = new DirectoryController(new User("Admin", "Admin", Roles.ADMIN));
        boolean expResult = false;
        boolean result = instance.pasteDirectory(parentDirectory, directoryToPaste);
        assertEquals(expResult, result);

    }

    /**
     * Test of copyDirectory method, of class DirectoryController.
     */
    @Test
    public void testCopyDirectory() {
        System.out.println("copyDirectory");
        String name = "DirectoryParent";
        String path = "/DirectoryParent";
        List<Permissions> l = new ArrayList<Permissions>();
        Permissions e = new Permissions(Roles.ADMIN, TypeOfPermission.FullControll);
        l.add(e);
        Directory parentDirectory = new Directory(name, new ArrayList<File>(), new ArrayList<Directory>(), path, l);
        Directory childDirectory = new Directory("ChildDirectory", new ArrayList<File>(), new ArrayList<Directory>(), "DirectoryParent/ChildDirectory", l);
        parentDirectory.getChildrenDirectory().add(childDirectory);
        DirectoryController instance = new DirectoryController(new User("Admin", "Admin", Roles.ADMIN));
        Directory expResult = childDirectory;
        Directory result = instance.copyDirectory(parentDirectory, "ChildDirectory");
        assertEquals(expResult, result);

    }

    /**
     * Test of copyDirectory method, of class DirectoryController.
     */
    @Test
    public void testCopyDirectoryFail() {
        System.out.println("copyDirectoryFail");
        String name = "DirectoryParent";
        String path = "/DirectoryParent";
        List<Permissions> l = new ArrayList<Permissions>();
        Permissions e = new Permissions(Roles.ADMIN, TypeOfPermission.FullControll);
        l.add(e);
        Directory parentDirectory = new Directory(name, new ArrayList<File>(), new ArrayList<Directory>(), path, l);
        Directory childDirectory = new Directory("ChildDirectory", new ArrayList<File>(), new ArrayList<Directory>(), "DirectoryParent/ChildDirectory", l);
        parentDirectory.getChildrenDirectory().add(childDirectory);
        DirectoryController instance = new DirectoryController(new User("Admin", "Admin", Roles.ADMIN));
        Directory expResult = null;
        Directory result = instance.copyDirectory(parentDirectory, "ChildDirectory123");
        assertEquals(expResult, result);

    }

    /**
     * Test of findDirectory method, of class DirectoryController.
     */
    @Test
    public void testFindDirectory() {
        System.out.println("findDirectory");
        String name = "DirectoryParent";
        String path = "/DirectoryParent";
        List<Permissions> l = new ArrayList<Permissions>();
        Permissions e = new Permissions(Roles.ADMIN, TypeOfPermission.FullControll);
        l.add(e);
        Directory parentDirectory = new Directory(name, new ArrayList<File>(), new ArrayList<Directory>(), path, l);
        Directory childDirectory = new Directory("ChildDirectory", new ArrayList<File>(), new ArrayList<Directory>(), "DirectoryParent/ChildDirectory", l);
        parentDirectory.getChildrenDirectory().add(childDirectory);
        DirectoryController instance = new DirectoryController(new User("Admin", "Admin", Roles.ADMIN));
        Directory expResult = childDirectory;
        Directory result = instance.findDirectory(parentDirectory, "ChildDirectory");
        assertEquals(expResult, result);

    }

    /**
     * Test of findDirectory method, of class DirectoryController.
     */
    @Test
    public void testFindDirectoryFail() {
        System.out.println("findDirectoryFail");
        String name = "DirectoryParent";
        String path = "/DirectoryParent";
        List<Permissions> l = new ArrayList<Permissions>();
        Permissions e = new Permissions(Roles.ADMIN, TypeOfPermission.FullControll);
        l.add(e);
        Directory parentDirectory = new Directory(name, new ArrayList<File>(), new ArrayList<Directory>(), path, l);
        Directory childDirectory = new Directory("ChildDirectory", new ArrayList<File>(), new ArrayList<Directory>(), "DirectoryParent/ChildDirectory", l);
        parentDirectory.getChildrenDirectory().add(childDirectory);
        DirectoryController instance = new DirectoryController(new User("Admin", "Admin", Roles.ADMIN));
        Directory expResult = null;
        Directory result = instance.findDirectory(parentDirectory, "ChildDirectoryfsa");
        assertEquals(expResult, result);

    }

    /**
     * Test of copyFile method, of class DirectoryController.
     */
    @Test
    public void testCopyFile() {
        System.out.println("copyFile");
        String name = "DirectoryParent";
        String path = "/DirectoryParent";
        List<Permissions> l = new ArrayList<Permissions>();
        Permissions e = new Permissions(Roles.ADMIN, TypeOfPermission.FullControll);
        l.add(e);
        Directory parentDirectory = new Directory(name, new ArrayList<File>(), new ArrayList<Directory>(), path, l);
        byte[] b = new byte[5];
        File file = new File("FileTest.txt", b, "DirectoryParent/FileTest", l);
        parentDirectory.getChildrenFile().add(file);
        DirectoryController instance = new DirectoryController(new User("Admin", "Admin", Roles.ADMIN));
        File expResult = file;
        File result = instance.copyFile(parentDirectory, "FileTest");
        assertEquals(expResult, result);

    }

    /**
     * Test of copyFile method, of class DirectoryController.
     */
    @Test
    public void testCopyFileFail() {
        System.out.println("copyFileFail");
        String name = "DirectoryParent";
        String path = "/DirectoryParent";
        List<Permissions> l = new ArrayList<Permissions>();
        Permissions e = new Permissions(Roles.ADMIN, TypeOfPermission.FullControll);
        l.add(e);
        Directory parentDirectory = new Directory(name, new ArrayList<File>(), new ArrayList<Directory>(), path, l);
        byte[] b = new byte[5];
        File file = new File("FileTest.txt", b, "DirectoryParent/FileTest", l);
        parentDirectory.getChildrenFile().add(file);
        DirectoryController instance = new DirectoryController(new User("Admin", "Admin", Roles.ADMIN));
        File expResult = null;
        File result = instance.copyFile(parentDirectory, "FileTest123");
        assertEquals(expResult, result);

    }

    /**
     * Test of pasteFile method, of class DirectoryController.
     */
    @Test
    public void testPasteFile() {
        System.out.println("pasteFile");
        String name = "DirectoryParent";
        String path = "/DirectoryParent";
        List<Permissions> l = new ArrayList<Permissions>();
        Permissions e = new Permissions(Roles.ADMIN, TypeOfPermission.FullControll);
        l.add(e);
        Directory parentDirectory = new Directory(name, new ArrayList<File>(), new ArrayList<Directory>(), path, l);
        byte[] b = new byte[5];
        File fileToPaste = new File("FileTest.txt", b, "DirectoryParent/FileTest", l);
        DirectoryController instance = new DirectoryController(new User("Admin", "Admin", Roles.ADMIN));
        boolean expResult = true;
        boolean result = instance.pasteFile(parentDirectory, fileToPaste);
        assertEquals(expResult, result);
   
    }
    
       /**
     * Test of pasteFile method, of class DirectoryController.
     */
    @Test
    public void testPasteFileFail() {
        System.out.println("pasteFileFail");
        String name = "DirectoryParent";
        String path = "/DirectoryParent";
        List<Permissions> l = new ArrayList<Permissions>();
        Permissions e = new Permissions(Roles.USER, TypeOfPermission.FullControll);
        l.add(e);
        Directory parentDirectory = new Directory(name, new ArrayList<File>(), new ArrayList<Directory>(), path, l);
        byte[] b = new byte[5];
        File fileToPaste = new File("FileTest.txt", b, "DirectoryParent/FileTest", l);
        DirectoryController instance = new DirectoryController(new User("Admin", "Admin", Roles.ADMIN));
        boolean expResult = false;
        boolean result = instance.pasteFile(parentDirectory, fileToPaste);
        assertEquals(expResult, result);
   
    }

    /**
     * Test of cutFile method, of class DirectoryController.
     */
    @Test
    public void testCutFile() {
        System.out.println("cutFile");
        String name = "DirectoryParent";
        String path = "/DirectoryParent";
        List<Permissions> l = new ArrayList<Permissions>();
        Permissions e = new Permissions(Roles.ADMIN, TypeOfPermission.FullControll);
        l.add(e);
        Directory parentDirectory = new Directory(name, new ArrayList<File>(), new ArrayList<Directory>(), path, l);
        byte[] b = new byte[5];
        File file = new File("FileTest.txt", b, "DirectoryParent/FileTest", l);
        parentDirectory.getChildrenFile().add(file);
        DirectoryController instance = new DirectoryController(new User("Admin", "Admin", Roles.ADMIN));
        File expResult = file;
        File result = instance.cutFile(parentDirectory, "FileTest");
        assertEquals(expResult, result);

    }

    /**
     * Test of cutFile method, of class DirectoryController.
     */
    @Test
    public void testCutFileFail() {
        System.out.println("cutFileFail");
        String name = "DirectoryParent";
        String path = "/DirectoryParent";
        List<Permissions> l = new ArrayList<Permissions>();
        Permissions e = new Permissions(Roles.ADMIN, TypeOfPermission.FullControll);
        l.add(e);
        Directory parentDirectory = new Directory(name, new ArrayList<File>(), new ArrayList<Directory>(), path, l);
        byte[] b = new byte[5];
        File file = new File("FileTest.txt", b, "DirectoryParent/FileTest", l);
        parentDirectory.getChildrenFile().add(file);
        DirectoryController instance = new DirectoryController(new User("Admin", "Admin", Roles.ADMIN));
        File expResult = null;
        File result = instance.cutFile(parentDirectory, "NonExisting");
        assertEquals(expResult, result);

    }

 


}
