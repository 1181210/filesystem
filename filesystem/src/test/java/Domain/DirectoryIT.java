/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Domain;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author user
 */
public class DirectoryIT {

    public DirectoryIT() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    /**
     *
     *
     * /**
     * Test of addDirectory method, of class Directory.
     */
    @Test
    public void testAddDirectoryCorrect() {
        System.out.println("addDirectory");
        List<Permissions> l = new ArrayList<Permissions>();
        Permissions e = new Permissions(Roles.USER, TypeOfPermission.Write);
        l.add(e);
        Directory parent = new Directory("DirectoryParent", new ArrayList<File>(), new ArrayList<Directory>(), "/DirectoryParent", l);
        Directory child = new Directory("DirectoryChild", new ArrayList<File>(), new ArrayList<Directory>(), "/DirectoryParent/DirectoryChild", l);
        boolean expResult = true;
        boolean result = parent.addDirectory(child);
        assertEquals(expResult, result);

    }

    /**
     * Test of addFile method, of class Directory.
     */
    @Test
    public void testAddFile() {
        System.out.println("addFile");
        String fileName = "FileChild";
        List<Permissions> l = new ArrayList<Permissions>();
        Permissions e = new Permissions(Roles.USER, TypeOfPermission.Write);
        l.add(e);
          String content="123";
        
        byte[] b1 = content.getBytes(StandardCharsets.UTF_8);
        File fileChild= new File("FildChild.txt", b1 , "DirectoryParent/FileChild", l);
        Directory parent = new Directory("DirectoryParent", new ArrayList<File>(), new ArrayList<Directory>(), "/DirectoryParent", l);
        boolean expResult = true;
        boolean result = parent.addFile(fileChild);
        assertEquals(expResult, result);
   
    }

    /**
     * Test of removeDirectory method, of class Directory.
     */
    @Test
    public void testRemoveDirectoryCorrect() {
        System.out.println("removeDirectoryCorrect");
        String dir = "DirectoryChild";
        List<Permissions> l = new ArrayList<Permissions>();
        Permissions e = new Permissions(Roles.USER, TypeOfPermission.Write);
        l.add(e);
        Directory parent = new Directory("DirectoryParent", new ArrayList<File>(), new ArrayList<Directory>(), "/DirectoryParent", l);
        Directory child = new Directory("DirectoryChild", new ArrayList<File>(), new ArrayList<Directory>(), "/DirectoryParent/DirectoryChild", l);
        parent.addDirectory(child);
        boolean expResult = true;
        boolean result = parent.removeDirectory(dir);
        assertEquals(expResult, result);

    }
    
     /**
     * Test of removeDirectory method, of class Directory.
     */
    @Test
    public void testRemoveDirectoryFalse() {
        System.out.println("removeDirectoryFalse");
        String dir = "DirectoryChild";
        List<Permissions> l = new ArrayList<Permissions>();
        Permissions e = new Permissions(Roles.USER, TypeOfPermission.Write);
        l.add(e);
        Directory parent = new Directory("DirectoryParent", new ArrayList<File>(), new ArrayList<Directory>(), "/DirectoryParent", l);
        boolean expResult = false;
        boolean result = parent.removeDirectory(dir);
        assertEquals(expResult, result);

    }

    /**
     * Test of removeFile method, of class Directory.
     */
    @Test
    public void testRemoveFileCorrect() {
        System.out.println("removeFileCorrect");
        String fileName = "FileChild";
        List<Permissions> l = new ArrayList<Permissions>();
        Permissions e = new Permissions(Roles.USER, TypeOfPermission.Write);
        l.add(e);
         String content="123";
        
        byte[] b1 = content.getBytes(StandardCharsets.UTF_8);
       
        File fileChild= new File("FileChild.txt", b1 , "DirectoryParent/FileChild", l);
        Directory parent = new Directory("DirectoryParent", new ArrayList<File>(), new ArrayList<Directory>(), "/DirectoryParent", l);
        parent.addFile(fileChild);
        boolean expResult = true;
        boolean result = parent.removeFile(fileName);
        assertEquals(expResult, result);
    }
    
      /**
     * Test of removeFile method, of class Directory.
     */
    @Test
    public void testRemoveFileFail() {
        System.out.println("removeFileFail");
        String fileName = "";
        List<Permissions> l = new ArrayList<Permissions>();
        Permissions e = new Permissions(Roles.USER, TypeOfPermission.Write);
        l.add(e);
        Directory parent = new Directory("DirectoryParent", new ArrayList<File>(), new ArrayList<Directory>(), "/DirectoryParent", l);
        boolean expResult = false;
        boolean result = parent.removeFile(fileName);
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class Directory.
     */
    @Test
    public void testEquals() {
        System.out.println("testEqualsTrue");
        
        List<Permissions> l = new ArrayList<Permissions>();
        Permissions e = new Permissions(Roles.USER, TypeOfPermission.Write);
        l.add(e);
        Directory aux= new Directory ("DirectoryParent", new ArrayList<File>(), new ArrayList<Directory>(), "/DirectoryParent", l);
        Object obj = (Directory) aux;
        Directory parent = new Directory("DirectoryParent", new ArrayList<File>(), new ArrayList<Directory>(), "/DirectoryParent", l);
        boolean expResult = true;
        boolean result = parent.equals(obj);
        assertEquals(expResult, result);
     
    }
    
        /**
     * Test of equals method, of class Directory.
     */
    @Test
    public void testEqualsFalse() {
        System.out.println("testEqualsFalse");
        Object obj = "";
        List<Permissions> l = new ArrayList<Permissions>();
        Permissions e = new Permissions(Roles.USER, TypeOfPermission.Write);
        l.add(e);
        Directory parent = new Directory("DirectoryParent", new ArrayList<File>(), new ArrayList<Directory>(), "/DirectoryParent", l);
        boolean expResult = false;
        boolean result = parent.equals(obj);
        assertEquals(expResult, result);
     
    }

}
