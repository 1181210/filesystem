/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import Bootstrap.Bootstrap;
import Controller.UserController;
import Domain.Disk;
import Users.User;
import java.io.IOException;
import static java.lang.System.exit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author user
 */
public class LoginMenuUI {
    private User user;
    
    UserController controller;
    
    public LoginMenuUI(UserController  controller){
        this.controller=controller;
    }

    public User show() {
        Scanner in = new Scanner (System.in);
        int maxTries = 3;
        User u1 = null;
        System.out.println("Administrator test data: Username: Admin; Password: Admin;");
        System.out.println("------------User Login------------");
        String username;
        String password;
        for (int i = maxTries; i > 0; --i) {
            System.out.println("Username:");
            username = in.nextLine();
            System.out.println("Password:");
            password = in.nextLine();
            u1 = controller.findUser(username, password);
            if (u1 == null) {
                
                System.out.println(i - 1 + " Tries left.");
            } else {
                System.out.println("Login successfully!");
                break;
            }
        } 
        return u1;
    }
}
