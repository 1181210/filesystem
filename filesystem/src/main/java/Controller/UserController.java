/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Domain.Roles;
import Users.User;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author David Almeida
 */
public class UserController {

    List<User> usersList;
    User admin;

    public UserController() {
        usersList = new ArrayList<>();
        admin=new User("Admin","Admin",Roles.ADMIN);
        usersList.add(admin);
    }

    public User findUser(String username, String password) {
        for (User u : usersList) {
            if (u.getUsername().equalsIgnoreCase(username)) {
                if (u.getPassword().equalsIgnoreCase(password)) {
                    return u;
                }
            }
        }
        return null;
    }
    
    
    public User registerUser(String username,String password){
        User u= new User(username,password,Roles.USER);
        this.usersList.add(u);
        return u;
    }

}
