/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import Bootstrap.Bootstrap;
import Controller.UserController;
import Domain.Disk;
import Domain.File;
import Domain.Roles;
import Users.User;
import java.io.IOException;
import static java.lang.System.exit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author user
 */
public class MainMenuUI {
     
    private UserController controller ;
    
 public MainMenuUI() {
      this.controller=new UserController();
    }

    public void show() throws  IOException, Exception {

   
        System.out.print(System.lineSeparator()
                + Calendar.getInstance().getTime()
                + System.lineSeparator());

        System.out.println("## FILE SYSTEM! ##" + System.lineSeparator());
        List<String> options = new ArrayList<String>();

        options.add("Log in");
        options.add("Register As A Client");

        Scanner scanIn = new Scanner(System.in);

        String choice;
        do {

            System.out.println("Please choose your option:");
            UIUtils.presentsList(options, "");
            choice = scanIn.nextLine();

            switch (choice) {
                case "0":
                    System.out.println("Bye Bye :D");
                    
                    exit(0);
                    break;
                case "1":
                    User user = new LoginMenuUI(this.controller).show();
                    loginCase(user);
                    break;
                case "2":
                    this.controller=new RegisterMenuUI(this.controller).show();
                    break;

                default:
                    System.out.println("Please introduce a valid number!");

            }
        } while (!choice.equals("0"));
    }

    public void loginCase(User user) throws  Exception {
        if (user.getRole().equals(Roles.USER)) {
            new AdminMenuUI(user).show();
        }else{
            new AdminMenuUI(user).show();
        }
    }
    
}
