/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Domain.Directory;
import Domain.File;
import Domain.Permissions;
import Domain.Roles;
import Domain.TypeOfPermission;
import Users.User;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author user
 */
public class FileControllerIT {

    public FileControllerIT() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of verifyPermissions method, of class FileController.
     */
    @Test
    public void testVerifyPermissions() {
        System.out.println("verifyPermissions");
        String name = "newDir";
        String path = "/DirectoryParent/newDir";
        List<Permissions> l = new ArrayList<Permissions>();
        Permissions e = new Permissions(Roles.ADMIN, TypeOfPermission.FullControll);
        l.add(e);
        Directory parentDirectory = new Directory(name, new ArrayList<File>(), new ArrayList<Directory>(), path, l);
        FileController instance = new FileController(new User("Admin", "Admin", Roles.ADMIN));
        boolean expResult = true;
        boolean result = instance.verifyPermissions(parentDirectory);
        assertEquals(expResult, result);
    }

    /**
     * Test of verifyPermissions method, of class FileController.
     */
    @Test
    public void testVerifyPermissionsFail() {
        System.out.println("verifyPermissionsFail");
        String name = "newDir";
        String path = "/DirectoryParent/newDir";
        List<Permissions> l = new ArrayList<Permissions>();
        Permissions e = new Permissions(Roles.USER, TypeOfPermission.FullControll);
        l.add(e);
        Directory parentDirectory = new Directory(name, new ArrayList<File>(), new ArrayList<Directory>(), path, l);
        FileController instance = new FileController(new User("Admin", "Admin", Roles.ADMIN));
        boolean expResult = false;
        boolean result = instance.verifyPermissions(parentDirectory);
        assertEquals(expResult, result);
    }

    /**
     * Test of verifyFilePermissions method, of class FileController.
     */
    @Test
    public void testVerifyFilePermissions() {
        System.out.println("verifyFilePermissions");
        List<Permissions> l = new ArrayList<Permissions>();
        Permissions e = new Permissions(Roles.ADMIN, TypeOfPermission.FullControll);
        l.add(e);
        byte[] content = new byte[5];
        FileController instance = new FileController(new User("Admin", "Admin", Roles.ADMIN));
        File file = new File("File.txt", content, "/File", l);
        boolean expResult = true;
        boolean result = instance.verifyFilePermissions(file);
        assertEquals(expResult, result);
    }

    /**
     * Test of verifyFilePermissions method, of class FileController.
     */
    @Test
    public void testVerifyFilePermissionsFalse() {
        System.out.println("verifyFilePermissionsFalse");
        List<Permissions> l = new ArrayList<Permissions>();
        Permissions e = new Permissions(Roles.USER, TypeOfPermission.FullControll);
        l.add(e);
        byte[] content = new byte[5];
        FileController instance = new FileController(new User("Admin", "Admin", Roles.ADMIN));
        File file = new File("File.txt", content, "/File", l);
        boolean expResult = false;
        boolean result = instance.verifyFilePermissions(file);
        assertEquals(expResult, result);
    }

    /**
     * Test of verifyFilePermissionsAdministrator method, of class
     * FileController.
     */
    @Test
    public void testVerifyFilePermissionsAdministrator() {
        System.out.println("verifyFilePermissionsAdministrator");
        List<Permissions> l = new ArrayList<Permissions>();
        Permissions e = new Permissions(Roles.ADMIN, TypeOfPermission.FullControll);
        l.add(e);
        byte[] content = new byte[5];
        FileController instance = new FileController(new User("Admin", "Admin", Roles.ADMIN));
        File file = new File("File.txt", content, "/File", l);
        boolean expResult = true;
        boolean result = instance.verifyFilePermissionsAdministrator(file);
        assertEquals(expResult, result);

    }

    /**
     * Test of verifyFilePermissionsAdministrator method, of class
     * FileController.
     */
    @Test
    public void testVerifyFilePermissionsAdministratorFail() {
        System.out.println("verifyFilePermissionsAdministratorFail");
        List<Permissions> l = new ArrayList<Permissions>();
        Permissions e = new Permissions(Roles.USER, TypeOfPermission.FullControll);
        l.add(e);
        byte[] content = new byte[5];
        FileController instance = new FileController(new User("Admin", "Admin", Roles.ADMIN));
        File file = new File("File.txt", content, "/File", l);
        boolean expResult = false;
        boolean result = instance.verifyFilePermissionsAdministrator(file);
        assertEquals(expResult, result);

    }

    /**
     * Test of addFile method, of class FileController.
     */
    @Test
    public void testAddFile() {
        System.out.println("addFile");
        String name = "DirectoryParent";
        String path = "/DirectoryParent";
        List<Permissions> l = new ArrayList<Permissions>();
        Permissions e = new Permissions(Roles.ADMIN, TypeOfPermission.FullControll);
        l.add(e);
        Directory parentDirectory = new Directory(name, new ArrayList<File>(), new ArrayList<Directory>(), path, l);
        FileController instance = new FileController(new User("Admin", "Admin", Roles.ADMIN));
        byte[] content = new byte[5];
        File file = new File("File.txt", content, "/File", l);
        boolean expResult = true;
        boolean result = instance.addFile(parentDirectory, file);
        assertEquals(expResult, result);

    }

    /**
     * Test of addFile method, of class FileController.
     */
    @Test
    public void testAddFileFail() {
        System.out.println("addFileFail");
        String name = "DirectoryParent";
        String path = "/DirectoryParent";
        List<Permissions> l = new ArrayList<Permissions>();
        Permissions e = new Permissions(Roles.ADMIN, TypeOfPermission.Read);
        l.add(e);
        Directory parentDirectory = new Directory(name, new ArrayList<File>(), new ArrayList<Directory>(), path, l);
        FileController instance = new FileController(new User("Admin", "Admin", Roles.ADMIN));
        byte[] content = new byte[5];
        File file = new File("File.txt", content, "/File", l);
        boolean expResult = false;
        boolean result = instance.addFile(parentDirectory, file);
        assertEquals(expResult, result);

    }

    /**
     * Test of changePermissions method, of class FileController.
     */
    @Test
    public void testChangePermissions() {
        System.out.println("changePermissions");
        String name = "DirectoryParent";
        String path = "/DirectoryParent";
        List<Permissions> l = new ArrayList<Permissions>();
        Permissions e = new Permissions(Roles.ADMIN, TypeOfPermission.FullControll);
        l.add(e);
        Directory parentDirectory = new Directory(name, new ArrayList<File>(), new ArrayList<Directory>(), path, l);
        FileController instance = new FileController(new User("Admin", "Admin", Roles.ADMIN));
        byte[] content = new byte[5];
        File file = new File("File.txt", content, "/DirectoryParent/File", l);
        parentDirectory.getChildrenFile().add(file);
        List<Permissions> listNewPermissions = new ArrayList<Permissions>();
        Permissions e1 = new Permissions(Roles.ADMIN, TypeOfPermission.Write);
        boolean expResult = true;
        boolean result = instance.changePermissions(parentDirectory, "File", listNewPermissions);
        assertEquals(expResult, result);
    }

    /**
     * Test of changePermissions method, of class FileController.
     */
    @Test
    public void testChangePermissionsFail() {
        System.out.println("changePermissionsFail");
        String name = "DirectoryParent";
        String path = "/DirectoryParent";
        List<Permissions> l = new ArrayList<Permissions>();
        Permissions e = new Permissions(Roles.ADMIN, TypeOfPermission.FullControll);
        l.add(e);
        Directory parentDirectory = new Directory(name, new ArrayList<File>(), new ArrayList<Directory>(), path, l);
        FileController instance = new FileController(new User("Admin", "Admin", Roles.ADMIN));
        byte[] content = new byte[5];
        File file = new File("File.txt", content, "/DirectoryParent/File", l);
        parentDirectory.getChildrenFile().add(file);
        List<Permissions> listNewPermissions = new ArrayList<Permissions>();
        Permissions e1 = new Permissions(Roles.ADMIN, TypeOfPermission.Write);
        boolean expResult = false;
        boolean result = instance.changePermissions(parentDirectory, "Filet", listNewPermissions);
        assertEquals(expResult, result);
    }

    /**
     * Test of changeContent method, of class FileController.
     */
    @Test
    public void testChangeContent() {
        System.out.println("changeContent");
        String name = "DirectoryParent";
        String path = "/DirectoryParent";
        List<Permissions> l = new ArrayList<Permissions>();
        Permissions e = new Permissions(Roles.ADMIN, TypeOfPermission.FullControll);
        l.add(e);
        Directory parentDirectory = new Directory(name, new ArrayList<File>(), new ArrayList<Directory>(), path, l);
        FileController instance = new FileController(new User("Admin", "Admin", Roles.ADMIN));
        byte[] content = new byte[5];
        File file = new File("File.txt", content, "/DirectoryParent/File", l);
        parentDirectory.getChildrenFile().add(file);
        String new_content = "123";
        byte[] b1 = new_content.getBytes(StandardCharsets.UTF_8);
        boolean expResult = true;
        boolean result = instance.changeContent(parentDirectory, "File", b1);
        assertEquals(expResult, result);

    }

    /**
     * Test of changeContent method, of class FileController.
     */
    @Test
    public void testChangeContentFail() {
        System.out.println("changeContentFail");
        String name = "DirectoryParent";
        String path = "/DirectoryParent";
        List<Permissions> l = new ArrayList<Permissions>();
        Permissions e = new Permissions(Roles.ADMIN, TypeOfPermission.FullControll);
        l.add(e);
        Directory parentDirectory = new Directory(name, new ArrayList<File>(), new ArrayList<Directory>(), path, l);
        FileController instance = new FileController(new User("Admin", "Admin", Roles.ADMIN));
        byte[] content = new byte[5];
        File file = new File("File.txt", content, "/DirectoryParent/File", l);
        parentDirectory.getChildrenFile().add(file);
        String new_content = "123";
        byte[] b1 = new_content.getBytes(StandardCharsets.UTF_8);
        boolean expResult = false;
        boolean result = instance.changeContent(parentDirectory, "File23", b1);
        assertEquals(expResult, result);

    }

    /**
     * Test of findFile method, of class FileController.
     */
    @Test
    public void testFindFile() {
        System.out.println("findFile");
        String name = "DirectoryParent";
        String path = "/DirectoryParent";
        List<Permissions> l = new ArrayList<Permissions>();
        Permissions e = new Permissions(Roles.ADMIN, TypeOfPermission.FullControll);
        l.add(e);
        Directory parentDirectory = new Directory(name, new ArrayList<File>(), new ArrayList<Directory>(), path, l);
        FileController instance = new FileController(new User("Admin", "Admin", Roles.ADMIN));
        byte[] content = new byte[5];
        File file = new File("File.txt", content, "/DirectoryParent/File", l);
        parentDirectory.getChildrenFile().add(file);
        File expResult = file;
        File result = instance.findFile(parentDirectory, "File");
        assertEquals(expResult, result);

    }

    /**
     * Test of findFile method, of class FileController.
     */
    @Test
    public void testFindFileFail() {
        System.out.println("findFileFail");
        String name = "DirectoryParent";
        String path = "/DirectoryParent";
        List<Permissions> l = new ArrayList<Permissions>();
        Permissions e = new Permissions(Roles.ADMIN, TypeOfPermission.FullControll);
        l.add(e);
        Directory parentDirectory = new Directory(name, new ArrayList<File>(), new ArrayList<Directory>(), path, l);
        FileController instance = new FileController(new User("Admin", "Admin", Roles.ADMIN));
        byte[] content = new byte[5];
        File file = new File("File.txt", content, "/DirectoryParent/File", l);
        parentDirectory.getChildrenFile().add(file);
        File expResult = null;
        File result = instance.findFile(parentDirectory, "File123");
        assertEquals(expResult, result);

    }

    /**
     * Test of checkDetailsOfAFile method, of class FileController.
     */
    @Test
    public void testCheckDetailsOfAFile() {
        System.out.println("checkDetailsOfAFile");
         String name = "DirectoryParent";
        String path = "/DirectoryParent";
        List<Permissions> l = new ArrayList<Permissions>();
        Permissions e = new Permissions(Roles.ADMIN, TypeOfPermission.FullControll);
        l.add(e);
        Directory parentDirectory = new Directory(name, new ArrayList<File>(), new ArrayList<Directory>(), path, l);
        FileController instance = new FileController(new User("Admin", "Admin", Roles.ADMIN));
        byte[] content = new byte[5];
        File file = new File("File.txt", content, "/DirectoryParent/File", l);
        parentDirectory.getChildrenFile().add(file);
        boolean expResult = true;
        boolean result = instance.checkDetailsOfAFile(parentDirectory, "File");
        assertEquals(expResult, result);
    
    }
    
     /**
     * Test of checkDetailsOfAFile method, of class FileController.
     */
    @Test
    public void testCheckDetailsOfAFileFail() {
        System.out.println("checkDetailsOfAFileFail");
         String name = "DirectoryParent";
        String path = "/DirectoryParent";
        List<Permissions> l = new ArrayList<Permissions>();
        Permissions e = new Permissions(Roles.ADMIN, TypeOfPermission.FullControll);
        l.add(e);
        Directory parentDirectory = new Directory(name, new ArrayList<File>(), new ArrayList<Directory>(), path, l);
        FileController instance = new FileController(new User("Admin", "Admin", Roles.ADMIN));
        byte[] content = new byte[5];
        File file = new File("File.txt", content, "/DirectoryParent/File", l);
        parentDirectory.getChildrenFile().add(file);
        boolean expResult = false;
        boolean result = instance.checkDetailsOfAFile(parentDirectory, "File123");
        assertEquals(expResult, result);
    
    }

}
