/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Domain;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author user
 */
public class Disk {
    
    /**
     * disk name
     */
    private String name;
    
    /**
     * disk maximum size
     */
    private int maxSize;
    
    /**
     * disk actual size
     */
    private int size;
    
    /**
     * root directory of the disk
     */
    private Directory root;
    
    /**
     * constructor of a disk
     * @param name
     * @param maxSize
     * @param size 
     */
    public Disk(String name, int maxSize, int size) {
        this.name = name;
        this.maxSize = maxSize;
        this.size = size;
        List<Permissions> listPermissions= new ArrayList<>(); 
        listPermissions.add(new Permissions(Roles.ADMIN,TypeOfPermission.FullControll));
        this.root = new Directory("Root",new ArrayList<File>(),new ArrayList<Directory>(),"/"+
                                                                                      this.name+"/Root", listPermissions);
    }

    public String getName() {
        return name;
    }

    public int getMaxSize() {
        return maxSize;
    }

    public int getSize() {
        return size;
    }

    public Directory getRoot() {
        return root;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setMaxSize(int maxSize) {
        this.maxSize = maxSize;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public void setRoot(Directory root) {
        this.root = root;
    }

}
