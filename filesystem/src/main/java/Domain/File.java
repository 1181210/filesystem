/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author David Almeida
 */
public class File {
    
    /**
     * variable that represents the name of the file
     */
    private String nameOfFile;
    
    /**
     * variable that represents the size of the file (float)
     */
    private long sizeOfFile;  
    /**
     * variable that represents the creation Date of the file
     */
    private Date creationDate;
    
    /**
     * Last modified date of the file
     */
    private Date modifyDate;
    
    /**
     * content of the file (ex: "ola")
     */
    private byte[] content;
    
    /**
     * extension of the file (ex: ".txt")
     */
    private String extension;
    
    /**
     * List with file permissions (user permissions and file permissions)
     */
    private List<Permissions> permissions;
    
    /**
     * path of the file
     */
    private String path;
    
    
            
   /**
    * Constructor of the file
    * @param nameOfFile
    * @param content
    * @param path
    * @param p 
    */
   public File(String nameOfFile, byte[] content,String path, List<Permissions> p){
        this.nameOfFile=nameOfFile.trim().split("\\.")[0];
        this.sizeOfFile= (long) content.length;
        Date timeDate= java.util.Calendar.getInstance().getTime();
        this.creationDate=timeDate;
        this.modifyDate=timeDate;
        this.content=content;
        this.extension=nameOfFile.trim().split("\\.")[1];
        this.path=path;
        this.permissions= p;
        
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }

    public String getExtension() {
        return extension;
    }
    
   public String getNameOfFile(){
       return this.nameOfFile;
   }

    public float getSizeOfFile() {
        return sizeOfFile;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public byte[] getContent() {
        return content;
    }

    public void setNameOfFile(String nameOfFile) {
        this.nameOfFile = nameOfFile;
    }

    public List<Permissions> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<Permissions> permissions) {
        this.permissions = permissions;
    }

    public void setSizeOfFile(Long sizeOfFile) {
        this.sizeOfFile = sizeOfFile;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }
   
   
    
    
}
