/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Users;

import Domain.Roles;

/**
 *
 * @author user
 */
public class User {
    
    /**
     * User username
     */
    private String username;
    
    /**
     * User password
     */
    private String password;
    
    /**
     * User role (user or admin)
     */
    private Roles role;
    
    /**
     * Constructor of a user
     * @param username
     * @param password
     * @param role 
     */
    public User(String username, String password, Roles role) {
        this.username = username;
        this.password = password;
        this.role = role;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public Roles getRole() {
        return role;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setRole(Roles role) {
        this.role = role;
    }
    
  
}
