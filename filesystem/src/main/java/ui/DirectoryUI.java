/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import Controller.DirectoryController;
import Domain.Directory;
import Domain.File;
import Domain.Permissions;
import Domain.TypeOfPermission;
import Users.User;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author user
 */
public class DirectoryUI {

    private final DirectoryController controller;
    private String nameOfDirectory;
    private User user;
    private Directory directory;
    private String newPath;
    private Directory cutCopyDirectory;
    private List<Permissions> listPermissions;
    private String nameOfFile;
    private File cutCopyFile;

    /**
     * Constructor of the UI
     *
     * @param d
     * @param user
     */
    public DirectoryUI(Directory d, User user) {
        this.controller = new DirectoryController(user);
        this.directory = d;
        this.user = user;
        this.listPermissions = new ArrayList<Permissions>();
        this.listPermissions.add(new Permissions(this.user.getRole(), TypeOfPermission.FullControll));
    }

    /**
     * Ask information to create a directory
     *
     * @param parent Directory
     * @return
     */
    public boolean askInformation(Directory parent) {
        this.directory = parent;
        UIUtils.printHead("Create new directory");
        Scanner scanner = new Scanner(System.in);
        String accept;

        System.out.println("Enter the name of the directory: ");
        this.nameOfDirectory = scanner.nextLine();

        String choice;
        TypeOfPermission typeOfPermission = null;
        do {

            System.out.println("Please choose the type of permissions that you want to the directory:");
            System.out.println(
                    "1 - Read\n"
                    + "2 - Write\n"
                    + "3 - Full Controll\n");

            choice = scanner.nextLine();

            switch (choice) {

                case "1":
                    typeOfPermission = TypeOfPermission.Read;
                    break;
                case "2":
                    typeOfPermission = TypeOfPermission.Write;
                    break;
                case "3":
                    typeOfPermission = TypeOfPermission.FullControll;
                    break;

            }
        } while (!choice.equals("1") && !choice.equals("2") && !choice.equals("3"));

        List<Permissions> l = new ArrayList<>();
        Permissions p = new Permissions(this.user.getRole(), typeOfPermission);
        l.add(p);

        displayInformation();

        System.out.println("Accept to Add Directory (y / n)");
        accept = scanner.next();
        while (!accept.equals("y") && !accept.equals("n")) {
            System.out.println("Accept to Add Directory (y / n)");
            accept = scanner.next();
        }
        if (accept.equals("y")) {
            Directory pDir = this.directory;
            Directory newDir = this.controller.createDirectory(this.nameOfDirectory, this.directory.getPath() + "/" + this.nameOfDirectory, l);
            newDir.setParentDirectory(pDir);
            //  this.directory.setPermissions(listPermissions);
            if (this.controller.addDirectory(this.directory, newDir)) {
           
                System.out.println("Created directory!");
                return true;
            } else {
               return false;
            }
           

        } else {

            System.out.println("Directory not created!");
            return false;
        }
    }

    /**
     * Ask information to delete a directory
     *
     * @param parent directory
     * @return
     */
    public boolean askInformationToDeleteDirectory(Directory parent) {
        this.directory = parent;
        UIUtils.printHead("Delete a directory");
        Scanner scanner = new Scanner(System.in);
        String accept;

        System.out.println("Enter the name of the directory that you want to delete: ");
        this.nameOfDirectory = scanner.nextLine();

        displayInformation();

        System.out.println("Accept to Delete the Directory and Every Item in it? (y / n)");
        accept = scanner.next();
        while (!accept.equals("y") && !accept.equals("n")) {
            System.out.println("Accept to Delete the Directory and Every Item in it (y / n)");
            accept = scanner.next();
        }
        if (accept.equals("y")) {

            if (this.controller.removeDirectory(this.directory, nameOfDirectory)) {
                System.out.println("Deleted directory " + this.nameOfDirectory + "!");
                return true;
            } else {
                return false;
            }
        } else {

            System.out.println("Directory not deleted!");
            return false;
        }
    }

    /**
     * Ask information to cut a directory
     *
     * @param parent directory
     * @return
     */
    public boolean askInformationToCutDirectory(Directory parent) {
        this.directory = parent;
        UIUtils.printHead("Cut a directory");
        Scanner scanner = new Scanner(System.in);
        String accept;

        System.out.println("Enter the name of the directory that you want to cut: ");
        this.nameOfDirectory = scanner.nextLine();

        displayInformation();

        System.out.println("Accept to Cut the Directory and Every Item in it? (y / n)");
        accept = scanner.next();
        while (!accept.equals("y") && !accept.equals("n")) {
            System.out.println("Accept to Cut the Directory and Every Item in it (y / n)");
            accept = scanner.next();
        }
        if (accept.equals("y")) {

            this.cutCopyDirectory = this.controller.cutDirectory(this.directory, nameOfDirectory);
            if (this.cutCopyDirectory != null) {
                System.out.println("Cut directory " + this.nameOfDirectory + "!");
                return true;
            } else {

                return false;
            }

        } else {

            System.out.println("Directory not cut!");
            return false;
        }
    }

    /**
     * Paste a directory
     *
     * @param parent directory
     * @return
     */
    public boolean pasteDirectory(Directory parent) {
        this.directory = parent;
        UIUtils.printHead("Paste a directory");
        Scanner scanner = new Scanner(System.in);
        String accept;

        System.out.println("Accept to Paste the Directory and Every Item in it? (y / n)");
        accept = scanner.next();
        while (!accept.equals("y") && !accept.equals("n")) {
            System.out.println("Accept to Paste the Directory and Every Item in it (y / n)");
            accept = scanner.next();
        }
        if (accept.equals("y")) {
            if (this.cutCopyDirectory != null) {
                if (this.controller.pasteDirectory(this.directory, this.cutCopyDirectory)) {
                    this.cutCopyDirectory.setParentDirectory(this.directory);
                    this.controller.setPath(this.directory, this.cutCopyDirectory);
                    System.out.println("Pasted directory " + this.cutCopyDirectory.getDirectoryName() + "!");
                    this.cutCopyDirectory = null;
                    return true;
                } else {
                    return false;
                }
            } else {

                System.out.println("You need to copy or cut a directory before this operation.");
                return false;
            }
        } else {
            System.out.println("Directory not pasted");
            return false;
        }

    }

    /**
     * Ask information to copy a directory
     *
     * @param parent directory
     * @return
     */
    public boolean askInformationToCopyADirectory(Directory parent) {
        this.directory = parent;
        UIUtils.printHead("Copy a directory");
        Scanner scanner = new Scanner(System.in);
        String accept;

        System.out.println("Enter the name of the directory that you want to copy: ");
        this.nameOfDirectory = scanner.nextLine();

        displayInformation();

        System.out.println("Accept to Copy the Directory and Every Item in it? (y / n)");
        accept = scanner.next();
        while (!accept.equals("y") && !accept.equals("n")) {
            System.out.println("Accept to Copy the Directory and Every Item in it (y / n)");
            accept = scanner.next();
        }
        if (accept.equals("y")) {

            this.cutCopyDirectory = this.controller.copyDirectory(this.directory, nameOfDirectory);
            if (this.cutCopyDirectory != null) {
                System.out.println("Copied directory " + this.cutCopyDirectory.getDirectoryName() + "!");
                return true;
            } else {

                return false;
            }
        } else {

            System.out.println("Directory not copied!");
            return false;
        }
    }

    /**
     * Ask information to delete a file
     *
     * @param parent directory
     * @return
     */
    public boolean askInformationToDeleteFile(Directory parent) {
        this.directory = parent;
        UIUtils.printHead("Delete a File");
        Scanner scanner = new Scanner(System.in);
        String accept;

        System.out.println("Enter the name of the file that you want to delete: (without extension) ");
        this.nameOfFile = scanner.nextLine();
       

        System.out.println("Accept to Delete the File? (y / n)");
        accept = scanner.next();
        while (!accept.equals("y") && !accept.equals("n")) {
            System.out.println("Accept to Delete the File? (y / n)");
            accept = scanner.next();
        }
        if (accept.equals("y")) {

            if (this.controller.removeFile(this.directory, this.nameOfFile)) {
                System.out.println("File " + this.nameOfFile + "!");
                return true;
            } else {
                return false;
            }

        } else {

            System.out.println("File not deleted!");
            return false;
        }
    }

    /**
     * Ask information to copy a file
     *
     * @param parent directory
     * @return
     */
    public boolean askInformationToCopyAFile(Directory parent) {
        this.directory = parent;
        UIUtils.printHead("Copy a file");
        Scanner scanner = new Scanner(System.in);
        String accept;

        System.out.println("Enter the name of the file that you want to copy: (without extension) ");
        this.nameOfFile = scanner.nextLine();

        System.out.println("Accept to Copy the File? (y / n)");
        accept = scanner.next();
        while (!accept.equals("y") && !accept.equals("n")) {
            System.out.println("Accept to Copy the File? (y / n)");
            accept = scanner.next();
        }
        if (accept.equals("y")) {

            this.cutCopyFile = this.controller.copyFile(this.directory, this.nameOfFile);
            if (this.cutCopyFile != null) {
                System.out.println("Copied file " + this.nameOfFile + "!");
                return true;
            } else {
                return false;
            }

        } else {

            System.out.println("File not copied!");
            return false;
        }
    }

    /**
     * Ask information to paste a file
     *
     * @param parent directory
     * @return
     */
    public boolean pasteFile(Directory parent) {
        this.directory = parent;
        UIUtils.printHead("Paste a file");
        Scanner scanner = new Scanner(System.in);
        String accept;

        System.out.println("Accept to Paste the File? (y / n)");
        accept = scanner.next();
        while (!accept.equals("y") && !accept.equals("n")) {
            System.out.println("Accept to Paste the File? (y / n)");
            accept = scanner.next();
        }
        if (accept.equals("y")) {
            if (this.cutCopyFile != null) {
                if (this.controller.pasteFile(this.directory, this.cutCopyFile)) {
                    this.controller.setPathFile(this.directory, this.cutCopyFile);
                    System.out.println("Pasted file " + this.cutCopyFile.getNameOfFile() + "!");
                    this.cutCopyFile = null;
                    return true;
                } else {
                    return false;
                }
            } else {
                System.out.println("Please copy or cut a file before do this operation");
                return false;
            }

        } else {

            System.out.println("File not pasted!");
            return false;
        }
    }

    /**
     * Ask information to cut a file
     *
     * @param parent directory
     * @return
     */
    public boolean askInformationToCutFile(Directory parent) {
        this.directory = parent;
        UIUtils.printHead("Cut a file");
        Scanner scanner = new Scanner(System.in);
        String accept;

        System.out.println("Enter the name of the file that you want to cut: (without extensions)");
        this.nameOfFile = scanner.nextLine();

        displayInformationForFiles();

        System.out.println("Accept to Cut the File? (y / n)");
        accept = scanner.next();
        while (!accept.equals("y") && !accept.equals("n")) {
            System.out.println("Accept to Cut the File? (y / n)");
            accept = scanner.next();
        }
        if (accept.equals("y")) {

            this.cutCopyFile = this.controller.cutFile(this.directory, this.nameOfFile);
            if (this.cutCopyFile != null) {
                System.out.println("Cut file " + this.nameOfFile + "!");
                return true;
            } else {
                return false;
            }

        } else {

            System.out.println("Directory not cut!");
            return false;
        }
    }

    /**
     * Display Head Info
     */
    private void displayInformation() {

        System.out.println("----------- Directory Information -----------");
        System.out.println("Directory name:" + this.nameOfDirectory);
        System.out.println("-----------------------------------------");

    }
    
     /**
     * Display Head Info
     */
    private void displayInformationForFiles() {

        System.out.println("----------- File Information -----------");
        System.out.println("File name:" + this.nameOfFile);
        System.out.println("-----------------------------------------");

    }

}
