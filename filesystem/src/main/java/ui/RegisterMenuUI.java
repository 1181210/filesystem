/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import Controller.UserController;
import Users.User;
import java.io.IOException;

/**
 *
 * @author David Almeida
 */
public class RegisterMenuUI {
    UserController controller;
    
    
    public RegisterMenuUI(UserController controller){
        this.controller=controller;
    }

    public UserController show() throws IOException {

        int maxTries = 3;
        boolean registered = false;
        System.out.println("\n Register User");

        String username = UIUtils.readLineFromConsole("Introduce your desired username:" + String.format("%n>"));
        String password = new String(UIUtils.readLineFromConsole("Introduce your desired Password:" + String.format("%n>")));
      
        
      for (int i =0; i<maxTries;i++){
            try {
                User u= controller.registerUser(username, password) ;
                registered= true;              
                break;
              
            }
        
            catch (IllegalArgumentException e1){
                System.out.println("Invalid arguments");
                break;
            }
          
        }

        if (registered) {
            System.out.println(String.format("%n") + "Successfully created the user.");
            return this.controller;
        } else {
            System.out.print( "We were unable to register you.");
            return this.controller;
        }
    }
}
