/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Domain;

import java.util.List;
import java.util.Objects;

/**
 *
 * @author user
 */
public class Directory  {
    
    /**
     * the parent directory of a directory
     */
    private Directory parentDirectory;
    
    /**
     * the name of the directory
     */
    private String directoryName;
    
    /**
     * List of files inside a directory
     */
    private List<File> file;
    
    /**
     * List of directories inside a directory
     */
    private List<Directory> childrenDirectory;
    
    /**
     * the path of a directory
     */
    private String path;
    
    /**
     * List of permissions of a directory (user permissions and directory permissions)
     */
    private List<Permissions> permissions;
    
    
    /**
     * Directory constructor
     * @param directoryName
     * @param childrenFile
     * @param childrenDirectory
     * @param path
     * @param permissions 
     */
    public Directory(String directoryName, List<File> childrenFile, List<Directory> childrenDirectory, String path, List<Permissions> permissions) {
        this.parentDirectory=null;
        this.directoryName = directoryName;
        this.file = childrenFile;
        this.childrenDirectory = childrenDirectory;
        this.path = path;
        this.permissions = permissions;
    }
    
    public void setParentDirectory(Directory parentDirectory) {
        this.parentDirectory = parentDirectory;
    }

    public void setFile(List<File> file) {
        this.file = file;
    }

    public Directory getParentDirectory() {
        return parentDirectory;
    }

    public List<File> getFile() {
        return file;
    }

    public void setDirectoryName(String directoryName) {
        this.directoryName = directoryName;
    }

    public void setChildrenFile(List<File> childrenFile) {
        this.file = childrenFile;
    }

    public void setChildrenDirectory(List<Directory> childrenDirectory) {
        this.childrenDirectory = childrenDirectory;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getDirectoryName() {
        return directoryName;
    }

    public List<File> getChildrenFile() {
        return file;
    }

    public List<Directory> getChildrenDirectory() {
        return childrenDirectory;
    }

    public String getPath() {
        return path;
    }

    public List<Permissions> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<Permissions> permissions) {
        this.permissions = permissions;
    }
    
    
    /**
     * Add a directory to the list of directories inside that directory
     * @param d
     * @return 
     */
    public boolean addDirectory(Directory d) {
        for (Directory dir : this.childrenDirectory) {
            if (dir.getDirectoryName().equals(d.getDirectoryName())) {
                return false;
            }
        }
       return this.getChildrenDirectory().add(d);
    }
    
    
    
    /**
     * Add a file to the list of files inside that directory
     * @param f
     * @return 
     */
     public boolean addFile(File f) {
        for (File f1 : this.getChildrenFile()) {
            if (f1.getNameOfFile().equals(f.getNameOfFile())) {
                return false;
            }
        }
       return this.getChildrenFile().add(f);

 
    
    }
    
     /**
      * Remove a directory in the list of directories inside the directory
      * @param dir
      * @return 
      */
    public boolean removeDirectory(String dir){
         for (Directory directory : this.childrenDirectory) {
            if (directory.getDirectoryName().equals(dir)) {
                return this.childrenDirectory.remove(directory);
              
            }
        }
        System.out.println("The indicated name of the directory does not exist");
        return false;
    }
    
    /**
     * Remove a file in the list of files inside the directory
     * @param dir
     * @return 
     */
     public boolean removeFile(String dir){
         for (File f : this.getChildrenFile()) {
            if (f.getNameOfFile().equals(dir)) {
                return this.getChildrenFile().remove(f);
              
            }
        }
        System.out.println("The indicated file name does not correspond to a existing file.");
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 11 * hash + Objects.hashCode(this.directoryName);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Directory other = (Directory) obj;
        if (!Objects.equals(this.directoryName, other.directoryName)) {
            return false;
        }
        return true;
    }

}
