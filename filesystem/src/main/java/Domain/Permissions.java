/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Domain;

/**
 *
 * @author user
 */
public class Permissions {
    
    /**
     * User role (User or Admin)
     */
    private Roles role;
    
    /**
     * Type of Permissions (Read, Write, Full Controll)
     */
    private TypeOfPermission typeOfPermission;
    
    /**
     * Constructor of the permissions
     * @param role
     * @param typeOfPermission 
     */
    public Permissions(Roles role, TypeOfPermission typeOfPermission) {
        this.role = role;
        this.typeOfPermission = typeOfPermission;
    }

    public Roles getRole() {
        return role;
    }

    public void setRole(Roles role) {
        this.role = role;
    }

    public TypeOfPermission getTypeOfPermission() {
        return typeOfPermission;
    }

    public void setTypeOfPermission(TypeOfPermission typeOfPermission) {
        this.typeOfPermission = typeOfPermission;
    }
    
    
    
    
}
