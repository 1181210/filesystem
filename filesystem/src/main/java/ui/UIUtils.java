/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import Domain.Directory;
import Domain.File;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.List;

/**
 *
 * @author user
 */
public class UIUtils {
    
    /**
     * Prints Method Head
     * @param useCase String 
     */
    public static void printHead(String useCase) {

        System.out.printf("%30s", "                         ### " + useCase.toUpperCase() + " ###   ");
        System.out.println();
    }
    
    
    static public void presentsList(List list,String sHeader)
    {
        System.out.println(sHeader);

        int index = 0;
        for (Object o : list)
        {
            index++;

            System.out.println(index + ". " + o.toString());
        }
        System.out.println("");
        System.out.println("0 - Cancel");
    } 
    
    /**
     * Read a Line from the console
     * @param strPrompt
     * @return 
     */
    static public String readLineFromConsole(String strPrompt)
    {
        try
        {
            System.out.println("\n" + strPrompt);

            InputStreamReader converter = new InputStreamReader(System.in);
            BufferedReader in = new BufferedReader(converter);

            return in.readLine();
        } catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }
    
    /**
     * List Directories inside a directory
     * @param parentDirectory directory
     */
    static public void showDirectories(Directory parentDirectory){
   
    
        for(Directory d: parentDirectory.getChildrenDirectory()){
            System.out.println("Directoy with name: "+d.getDirectoryName()+".");
        }
        
    }
    
    /**
     * Get a Directory giving a name 
     * @param parentDirectory directory
     * @param name string
     * @return 
     */
    static public Directory getDirectoryByName(Directory parentDirectory, String name){
        for(Directory d: parentDirectory.getChildrenDirectory()){
            if(d.getDirectoryName().equalsIgnoreCase(name)){
                return d; 
            }
        }
        return null;
    }
    
    /**
     * List Files Inside a Directory
     * @param parentDirectory directory
     */
    static public void showFiles(Directory parentDirectory){
   
    
        for(File f: parentDirectory.getChildrenFile()){
            System.out.println("File with name: "+f.getNameOfFile()+".");
        }
        
    }
    
}
