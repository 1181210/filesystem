/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import Controller.DirectoryController;
import Controller.FileController;
import Domain.Directory;
import Domain.File;
import Domain.Permissions;
import Domain.TypeOfPermission;
import Users.User;
import static java.lang.System.exit;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author user
 */
public class FileUI {

    private final DirectoryController controller;
    private final FileController controllerF;
    private String nameOfFile;
    private User user;
    private Directory directory;
    private String newPath;
    private Directory cutCopyDirectory;
    private List<Permissions> listPermissions;
    private byte[] contentOfFile;
    
    /**
     * Constructor of FileUI
     * @param d
     * @param user 
     */
    public FileUI(Directory d, User user) {
        this.controller = new DirectoryController(user);
        this.controllerF = new FileController(user);
        this.directory = d;
        this.user = user;
        this.listPermissions = new ArrayList<Permissions>();
        this.listPermissions.add(new Permissions(this.user.getRole(), TypeOfPermission.FullControll));
    }
    
    /**
     * Ask Information to create a file
     * @param parent directory
     * @return 
     */
    public boolean askInformation(Directory parent) {
        this.directory = parent;
     
        UIUtils.printHead("Create New File");
        Scanner scanner = new Scanner(System.in);
        String accept;

        System.out.println("Enter the name of the file: (format: name.extension) ");
        this.nameOfFile = scanner.nextLine();

        System.out.println("Enter the content that you want to write in the file: ");
        String s = scanner.nextLine();

        byte[] b1 = s.getBytes(StandardCharsets.UTF_8);
        this.contentOfFile = b1;

        String choice;
        TypeOfPermission typeOfPermission = null;
        do {

            System.out.println("Please choose the type of permissions that you want to the file:");
            System.out.println(
                    "1 - Read\n"
                    + "2 - Write\n"
                    + "3 - Full Controll\n");

            choice = scanner.nextLine();

            switch (choice) {

                case "1":
                    typeOfPermission = TypeOfPermission.Read;
                    break;
                case "2":
                    typeOfPermission = TypeOfPermission.Write;
                    break;
                case "3":
                    typeOfPermission = TypeOfPermission.FullControll;
                    break;
                default:
                 System.out.println("Please introduce a valid number!");
                 break;
            }
        } while (!choice.equals("1") && !choice.equals("2") && !choice.equals("3"));

        displayInformation();

        System.out.println("Accept to Add File (y / n)");
        accept = scanner.next();
        while (!accept.equals("y") && !accept.equals("n")) {
            System.out.println("Accept to Add File (y / n)");
            accept = scanner.next();
        }
        if (accept.equals("y")) {

            File newFile = this.controllerF.createFile(this.nameOfFile, this.contentOfFile, this.directory.getPath() + "/" + nameOfFile, typeOfPermission);

            if(this.controllerF.addFile(this.directory, newFile)){
                  System.out.println("Created file!");
                return true; 
            }else{
                System.out.println("File not created!");
                return false;
            }
           

        } else {

            System.out.println("File not created!");
            return false;
        }
    }
    
    /**
     * Ask Information to Change Permissions Of a File
     * @param parent directory
     * @return 
     */
    public boolean changePermissionsOfAFile(Directory parent) {
        this.directory = parent;
        this.directory.setPermissions(listPermissions);
        UIUtils.printHead("Change Permissions Of a File");
        Scanner scanner = new Scanner(System.in);
        String accept;
        System.out.println("Enter the name of the file that you want to edit the permissions (without extension) ");
        this.nameOfFile = scanner.nextLine();
        String choice;
        TypeOfPermission typeOfPermission = null;
        do {
            System.out.println("Please choose the new type of permissions that you want to the file:");
            System.out.println(
                    "1 - Read\n"
                    + "2 - Write\n"
                    + "3 - Full Controll\n");

            choice = scanner.nextLine();

            switch (choice) {

                case "1":
                    typeOfPermission = TypeOfPermission.Read;
                    break;
                case "2":
                    typeOfPermission = TypeOfPermission.Write;
                    break;
                case "3":
                    typeOfPermission = TypeOfPermission.FullControll;
                    break;
                default:
                     System.out.println("Please introduce a valid number!");
                     break;
                     
            }
        } while (!choice.equals("1") && !choice.equals("2") && !choice.equals("3"));

        List<Permissions> l = new ArrayList<>();
        Permissions p = new Permissions(this.user.getRole(), typeOfPermission);
        l.add(p);

        displayInformation();

        System.out.println("Accept to Edit the File Permissions? (y / n)");
        accept = scanner.next();
        while (!accept.equals("y") && !accept.equals("n")) {
            System.out.println("Accept to Edit the File Permissions? (y / n)");
            accept = scanner.next();
        }
        if (accept.equals("y")) {

            if (this.controllerF.changePermissions(this.directory, this.nameOfFile, l)){
                System.out.println("Edited Permissions of file " + this.nameOfFile + "!");
                return true;
            }else{
               return false;
            }

            

        } else {

            System.out.println("Permissions Not Edited!");
            return false;
        }
    }
    
    /**
     * Ask Information to Change the Content Of a File
     * @param parent directory
     * @return 
     */
     public boolean changeContentOfAFile(Directory parent) {
        this.directory = parent;
        UIUtils.printHead("Change Content Of a File");
        Scanner scanner = new Scanner(System.in);
        String accept;

        System.out.println("Enter the name of the file that you want to edit the content (without extension) ");
        this.nameOfFile = scanner.nextLine();
        
        System.out.println("Please enter the new content that you want in the file.");
        String content=scanner.nextLine();
        
        byte[] b1 = content.getBytes(StandardCharsets.UTF_8);
        this.contentOfFile = b1;

        displayInformation();

        System.out.println("Accept to Edit the File Content? (y / n)");
        accept = scanner.next();
        while (!accept.equals("y") && !accept.equals("n")) {
            System.out.println("Accept to Edit the File Content? (y / n)");
            accept = scanner.next();
        }
        if (accept.equals("y")) {

            if(this.controllerF.changeContent(this.directory, this.nameOfFile, b1)){
                 System.out.println("Edited Content of file " + this.nameOfFile + "!");
                 return true;
            }else{
              
                return false;
            }

            

        } else {

            System.out.println("Content Not Edited!");
            return false;
        }
    }
     
     /**
      * Ask Information to Check Details Of a File
      * @param parent directory
      * @return 
      */
    public boolean checkDetailsOfAFile(Directory parent) {
        this.directory = parent;
        this.directory.setPermissions(listPermissions);
        UIUtils.printHead("Check Details of a File");
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter the name of the file that you want to check the details: (without extension) ");
        this.nameOfFile = scanner.nextLine();

        if (this.controllerF.checkDetailsOfAFile(this.directory, this.nameOfFile)){
            return true;
        }else{
            return false;
        }
        
  

    }

     /**
      * Display Head Info
      */
    private void displayInformation() {

        System.out.println("----------- File Information -----------");
        System.out.println("File name:" + this.nameOfFile);
        System.out.println("-----------------------------------------");

    }
}
