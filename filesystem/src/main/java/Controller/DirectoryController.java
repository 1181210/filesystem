/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Domain.Directory;
import Domain.File;
import Domain.Permissions;
import Domain.TypeOfPermission;
import Users.User;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author user
 */
public class DirectoryController implements IController {

    private User sessionUser;

    public DirectoryController(User user) {
        this.sessionUser = user;
    }

    public Directory createDirectory(String name, String path, List<Permissions> permissions) {
        Directory d = new Directory(name, new ArrayList<File>(), new ArrayList<Directory>(), path, permissions);
        return d;
    }

    public boolean verifyPermissions(Directory parentDirectory) {
        for (Permissions p : parentDirectory.getPermissions()) {
            if (p.getRole() == this.sessionUser.getRole() && (p.getTypeOfPermission() == TypeOfPermission.FullControll
                    || p.getTypeOfPermission() == TypeOfPermission.Write)) {
                return true;

            }

        }
        return false;
    }

    public boolean addDirectory(Directory parentDirectory, Directory childDirectory) {
        boolean hasPermissions = verifyPermissions(parentDirectory);
        if (!hasPermissions) {
            System.out.println("You don't have the right permissions.");
            return false;
        }

        return parentDirectory.addDirectory(childDirectory);

    }

    public boolean removeDirectory(Directory parentDirectory, String childDirectory) {
        boolean hasPermissions = verifyPermissions(parentDirectory);
        if (!hasPermissions) {
            System.out.println("You don't have the right permissions.");
            return false;
        }
        return parentDirectory.removeDirectory(childDirectory);

    }

    public boolean removeFile(Directory parentDirectory, String childFile) {
        boolean hasPermissions = verifyPermissions(parentDirectory);
        if (!hasPermissions) {
            System.out.println("You don't have the right permissions to do this operation.");
            return false;
        }
        return parentDirectory.removeFile(childFile);

    }

    public Directory cutDirectory(Directory parentDirectory, String name) {

        boolean hasPermissions = verifyPermissions(parentDirectory);
        if (!hasPermissions) {
            System.out.println("You dont have the right permissions to do this operation.");
            return null;
        }

        Directory aux = findDirectory(parentDirectory, name);
        if (aux != null) {
            removeDirectory(parentDirectory, name);
            return aux;
        } else {
            System.out.println("Please indicate a valid directory name.");
            return null;
        }

    }

    public boolean pasteDirectory(Directory parentDirectory, Directory directoryToPaste) {
        boolean hasPermissions = verifyPermissions(parentDirectory);
        if (!hasPermissions) {
            System.out.println("You Dont have permissions to create this directory");
            return false;
        }

        return parentDirectory.addDirectory(directoryToPaste);

    }

    public Directory copyDirectory(Directory parentDirectory, String name) {
        boolean hasPermissions = verifyPermissions(parentDirectory);
        if (!hasPermissions) {
            System.out.println("You don't have the right permissions to do this operation.");
            return null;
        }
        Directory aux = findDirectory(parentDirectory, name);
        if (aux == null) {
            System.out.println("Please indicate a valid directory name.");
        }
        return aux;

    }

    public Directory findDirectory(Directory parentDirectory, String name) {
        Directory aux = null;
        for (Directory d : parentDirectory.getChildrenDirectory()) {
            if (d.getDirectoryName().equalsIgnoreCase(name)) {
                aux = d;
            }
        }
        return aux;
    }

    public File copyFile(Directory parentDirectory, String name) {
        boolean hasPermissions = verifyPermissions(parentDirectory);
        if (!hasPermissions) {
            System.out.println("Yout don't have the right permissions to do this operation!");
            return null;
        }
        File aux = null;
        for (File f : parentDirectory.getChildrenFile()) {
            if (f.getNameOfFile().equalsIgnoreCase(name)) {
                aux = f;
            }
        }
        if (aux == null) {
            System.out.println("The indicated file name does not correspond to a existing file.");
        }
        return aux;

    }

    public boolean pasteFile(Directory parentDirectory, File fileToPaste) {
        boolean hasPermissions = verifyPermissions(parentDirectory);
        if (!hasPermissions) {
            System.out.println("You Dont have permissions to create this directory.");
            return false;
        }

        return parentDirectory.addFile(fileToPaste);

    }

    public File cutFile(Directory parentDirectory, String name) {
        boolean hasPermissions = verifyPermissions(parentDirectory);
        if (!hasPermissions) {
            System.out.println("You don't have the right permissions to do this operation.");
            return null;
        }
        File aux = null;
        for (File f : parentDirectory.getChildrenFile()) {
            if (f.getNameOfFile().equalsIgnoreCase(name)) {
                aux = f;
            }
        }
        if (aux == null) {
            System.out.println("The indicated file name does not correspond to an existing file.");
            return null;
        }
        removeFile(parentDirectory, name);
        return aux;

    }

    public void setPath(Directory parentDirectory, Directory childDirectory) {
        childDirectory.setPath(parentDirectory.getPath() + "/" + childDirectory.getDirectoryName());
    }

    public void setPathFile(Directory parentDirectory, File file) {
        file.setPath(parentDirectory.getPath() + "/" + file.getNameOfFile());
    }
}
