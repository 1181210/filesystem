/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Domain.Directory;
import Domain.File;
import Domain.Permissions;
import Domain.Roles;
import Domain.TypeOfPermission;
import Users.User;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author user
 */
public class FileController implements IController {

    private File file;
    private User sessionUser;

    public FileController(User user) {
        this.sessionUser = user;

    }

    public File createFile(String name, byte[] content, String path, TypeOfPermission t) {

        Roles r = this.sessionUser.getRole();
        List<Permissions> lPermissions = new ArrayList<Permissions>();
        Permissions p = new Permissions(r, t);
        lPermissions.add(p);
        File f = new File(name, content, path, lPermissions);
        return f;
    }

    public boolean verifyPermissions(Directory parentDirectory) {
        for (Permissions p : parentDirectory.getPermissions()) {
            if (p.getRole() == this.sessionUser.getRole() && (p.getTypeOfPermission() == TypeOfPermission.FullControll
                    || p.getTypeOfPermission() == TypeOfPermission.Write)) {
                return true;

            }

        }
        return false;
    }

    public boolean verifyFilePermissions(File file) {
        for (Permissions p : file.getPermissions()) {
            if (p.getRole() == this.sessionUser.getRole() && (p.getTypeOfPermission() == TypeOfPermission.FullControll
                    || p.getTypeOfPermission() == TypeOfPermission.Write)) {
                return true;

            }

        }
        return false;
    }

    public boolean verifyFilePermissionsAdministrator(File file) {
        for (Permissions p : file.getPermissions()) {
            if (p.getRole() == this.sessionUser.getRole()) {
                return true;

            }

        }
        return false;
    }

    public boolean addFile(Directory parentDirectory, File file) {
        boolean hasPermissions = verifyPermissions(parentDirectory);
        if (!hasPermissions) {
            return false;
        }

        return parentDirectory.addFile(file);

    }

    public boolean changePermissions(Directory parentDirectory, String file, List<Permissions> list) {
        boolean hasPermissions = verifyPermissions(parentDirectory);
        if (!hasPermissions) {
            return false;
        }

        File foundFile = findFile(parentDirectory, file);

        

        if (foundFile != null) {
             hasPermissions = verifyFilePermissionsAdministrator(foundFile);
             if (!hasPermissions) {
                 System.out.println("You dont have the right permissions to change the permissions of the file");
                 return false;
             }
            foundFile.setPermissions(list);
            parentDirectory.removeFile(file);
            parentDirectory.getChildrenFile().add(foundFile);
            return true;
        } else {
            System.out.println("Permissions not set (please indicate a valid file name)");
            return false;
        }

    }

    public boolean changeContent(Directory parentDirectory, String file, byte[] content) {
        boolean hasPermissions = verifyPermissions(parentDirectory);
        if (!hasPermissions) {
            System.out.println("This Directory Does not have Write or Full Controll Permissions or You are not the owner of it");
            return false;
        }

        File foundFile = findFile(parentDirectory, file);

        if (foundFile != null) {
            hasPermissions = verifyFilePermissions(foundFile);
            if (!hasPermissions) {
                System.out.println("This File Does not have Write or Full Controll Permissions");
                return false;
            }
            foundFile.setContent(content);
            foundFile.setModifyDate(java.util.Calendar.getInstance().getTime());
            Long aux= new Long(content.length);
            foundFile.setSizeOfFile(aux);
            parentDirectory.removeFile(file);
            parentDirectory.getChildrenFile().add(foundFile);
            return true;
        } else {
            System.out.println("Content not set (please indicate a valid file name).");
            return false;
        }

    }

    public File findFile(Directory parentDarectory, String file) {
        File foundFile = null;
        for (File f : parentDarectory.getChildrenFile()) {
            if (f.getNameOfFile().equals(file)) {
                foundFile = f;
            }
        }
        return foundFile;
    }

    public boolean checkDetailsOfAFile(Directory parentDirectory, String file) {

        File foundFile = findFile(parentDirectory, file);

        if (foundFile != null) {
            System.out.println("File Name: " + foundFile.getNameOfFile() + ".");
            System.out.println("File Creation Date: " + foundFile.getCreationDate().toString() + " .");
            System.out.println("File Modify Date: " + foundFile.getModifyDate().toString() + " .");
            System.out.print("File Content: ");
            String string = new String(foundFile.getContent());
            System.out.println(string);
            System.out.println("File Size: " + foundFile.getSizeOfFile() + " .");
            for (Permissions p : foundFile.getPermissions()) {
                System.out.println("File Permission Role: " + p.getRole() + " .");
                System.out.println("File Permission Type Of Permission : " + p.getTypeOfPermission() + " .");
            }
            System.out.println("File extension: " + foundFile.getExtension() + " .");

            return true;
        } else {
            System.out.println("The indicated file name does not exist.");
            return false;
        }

    }

}
