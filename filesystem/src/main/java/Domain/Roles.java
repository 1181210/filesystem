/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Domain;

/**
 * Enum with the possible values of the Role (ADMIN, USER)
 * @author David Almeida
 */
public enum Roles {
    ADMIN, USER
}
